﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Flare_BattleShip_Interfaces;
using Flare_BattleShip_Models;

namespace Flare_BattleShip_GameLogic
{
    public class Game : IGame
    {
        private readonly IBattleBoard _board;
        private readonly IShip _ship;
        //can be made interactive 
        private int _boardSize = 10;

        public Game(IBattleBoard board, IShip ship)
        {
            _board = board;
            _ship = ship;
        }

        public void Start()
        {
            try
            {
                do
                {
                    Helper.WriteEmptyLine();

                    var isValidBoardSize = false;

                    do
                    {
                        Console.WriteLine("Please select board size between 1-10 : ");

                        isValidBoardSize = int.TryParse(Console.ReadLine(), out _boardSize) ?
                            Regex.IsMatch(_boardSize.ToString(), "^([1-9]|10)$") : false;

                    } while (!isValidBoardSize);
                    

                    Console.Clear();

                    Console.WriteLine("Creating board : ");
                    
                    var newBoard = _board.CreateBoard(_boardSize);

                    if (newBoard?.Rows == null)
                    {
                        Helper.WriteError("Unable to create board. Please restart the game.");
                        return;
                    }

                    Helper.WriteInfo("Board created successfully.");

                    Helper.WriteEmptyLine();

                    Helper.WriteInfo("Please select the ship co-ordinates  : ");

                    Helper.WriteWarning("E.g. : A1 to place ship in A1 or ");
                    Helper.WriteWarning("A1:A4 to place ship horizontally between A1 and A4 or ");
                    Helper.WriteWarning("A1:D1 to place ship vertically between A1 and D1 : ");

                    Helper.WriteEmptyLine();

                    Console.ForegroundColor = ConsoleColor.Yellow;

                    var isValidShip = false;
                    var validShipCordinates = new List<Coordinates>();
                    do
                    {
                        var validationResult = ValidateShipCoordinates(Console.ReadLine());
                        isValidShip = validationResult.Item1;

                        if (isValidShip)
                        {
                            validShipCordinates = validationResult.Item2;
                            break;
                        }
                        Helper.WriteInfo("Please select the ship co-ordinates  : ");

                    } while (!isValidShip);


                    _ship.AddShip(validShipCordinates, _boardSize);
                    
                    Helper.WriteInfo("Ship added successfully.");

                    Helper.WriteInfo("Let's start the game by attacking the co-ordinates one at a time.");
                    
                    var isShipDestroyed = false;

                    do
                    {
                        Helper.WriteEmptyLine();

                        Helper.WriteInfo("Please select the co-ordinate to attack : ");
                        
                        var attackXY = Console.ReadLine();

                        if (!Regex.IsMatch(attackXY, "^[A-Ja-j]([1-9]|10){1,2}$"))
                        {
                            Helper.WriteError("Attack Out Of the Board.");
                            continue;
                        }

                        var attackX = Helper.ConvertAlphaToInt(attackXY.Substring(0, 1));
                        var attackY = Convert.ToInt32(attackXY.Substring(1));

                        var attackResult = _ship.Attack(new Coordinates() { xCoordinate = attackX, yCoordinate = attackY });
                        isShipDestroyed = attackResult.Item2;
                        
                        if (attackResult.Item1 == ShotStatus.Hit)
                            Helper.WriteSuccess($"Shot At : {attackXY} Status : { attackResult.Item1} ");
                        else
                            Helper.WriteError($"Shot At : {attackXY} Status : { attackResult.Item1} ");

                    } while (!isShipDestroyed);

                    Console.ResetColor();

                    if (isShipDestroyed)
                        Helper.WriteSuccess("You Win !!! Ship has been destroyed.");

                    Helper.WriteEmptyLine();

                    Helper.WriteInfo("Do you wish to play again ? ");

                } while (Console.ReadKey(false).Key == ConsoleKey.Y);

                Helper.WriteEmptyLine();
                Helper.WriteInfo("Press any key to exit.");
                Console.ReadLine();
            }
            catch
            {
                Helper.WriteError("Error in game. Please restart.");
                Console.ReadLine();
            }
        }

        private Tuple<bool, List<Coordinates>> ValidateShipCoordinates(string shipCoordinates)
        {
            try
            {
                if (string.IsNullOrEmpty(shipCoordinates))
                    return new Tuple<bool, List<Coordinates>>(false, null);

                List<string> shipSpan = shipCoordinates.Split(':').ToList();

                if (shipSpan.Count > 2)
                    return new Tuple<bool, List<Coordinates>>(false, null);

                if (shipSpan.Count == 2)
                {
                    var shipStartingAt = shipSpan[0];
                    var shipEndingAt = shipSpan[1];

                    var shipStartX = Helper.ConvertAlphaToInt(shipStartingAt.Substring(0, 1));
                    var shipEndX = Helper.ConvertAlphaToInt(shipEndingAt.Substring(0, 1));

                    var shipStartY = Convert.ToInt32(shipStartingAt.Substring(1));
                    var shipEndY = Convert.ToInt32(shipEndingAt.Substring(1));

                    if ((shipStartX < 0 || shipStartX > _boardSize - 1 ||
                        shipEndX < 0 || shipEndX > _boardSize - 1) &&
                        (shipStartX == shipEndX || shipStartY == shipEndY))
                    {
                        Helper.WriteError("Invalid ship co-ordinates.");
                        return new Tuple<bool, List<Coordinates>>(false, null);
                    }

                    for (int i = shipStartX + 1; i < shipEndX; i++)
                    {
                        shipSpan.Add(string.Concat(Helper.ConvertIntToAlpha(i), shipStartY));
                    }

                    for (int i = shipStartY + 1; i < shipEndY; i++)
                    {
                        shipSpan.Add(string.Concat(shipStartX, Helper.ConvertIntToAlpha(i)));
                    }
                }

                var validCoordinates = new List<Coordinates>();

                foreach (var shipSpanCoordinate in shipSpan)
                {
                    if (shipSpanCoordinate.Length < 2)
                    {
                        Helper.WriteError("Invalid ship co-ordinates.");
                        return new Tuple<bool, List<Coordinates>>(false, null);
                    }

                    var shipX = Helper.ConvertAlphaToInt(shipSpanCoordinate.Substring(0, 1));
                    var shipY = Convert.ToInt32(shipSpanCoordinate.Substring(1));

                    if (!Validate(shipX, shipY))
                    {
                        Helper.WriteError("Invalid ship co-ordinates.");
                        return new Tuple<bool, List<Coordinates>>(false, null);
                    }

                    validCoordinates.Add(new Coordinates() { xCoordinate = shipX, yCoordinate = shipY });
                }

                return new Tuple<bool, List<Coordinates>>(true, validCoordinates);
            }
            catch
            {
                return new Tuple<bool, List<Coordinates>>(false, null);
            }
        }

        
        private bool Validate(int x, int y)
        {
            return !(x < 0 || x > _boardSize || y < 0 || y > _boardSize);
        }
    }
}
