﻿using System;
using System.Text.RegularExpressions;

namespace Flare_BattleShip_GameLogic
{
    public class Helper
    {
        public static int ConvertAlphaToInt(string a)
        {
            if (!Regex.IsMatch(a, @"^[a-jA-J]+$"))
                return -1;

            switch (a.ToUpper())
            {
                case "A":
                    return 0;

                case "B":
                    return 1;

                case "C":
                    return 2;

                case "D":
                    return 3;

                case "E":
                    return 4;

                case "F":
                    return 5;

                case "G":
                    return 6;

                case "H":
                    return 7;

                case "I":
                    return 8;

                case "J":
                    return 9;

                default:
                    return -1;
            }
        }

        public static string ConvertIntToAlpha(int i)
        {
            if (!Regex.IsMatch(i.ToString(), @"^[0-9]+$"))
                return null;

            switch (i)
            {
                case 0:
                    return "A";

                case 1:
                    return "B";

                case 2:
                    return "C";

                case 3:
                    return "D";

                case 4:
                    return "E";

                case 5:
                    return "F";

                case 6:
                    return "G";

                case 7:
                    return "H";

                case 8:
                    return "I";

                case 9:
                    return "J";

                default:
                    return null;
            }
        }

        public static void WriteInfo(string message)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void WriteSuccess(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void WriteError(string errorMessage)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(errorMessage);
            Console.ResetColor();
        }

        public static void WriteWarning(string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void WriteEmptyLine()
        {
            Console.WriteLine(Environment.NewLine);
        }
    }
}
