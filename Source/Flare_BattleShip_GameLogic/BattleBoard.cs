﻿using System;
using Flare_BattleShip_Interfaces;
using Flare_BattleShip_Models;

namespace Flare_BattleShip_GameLogic
{
    public class BattleBoard : IBattleBoard
    {        
        public Board CreateBoard(int boardSize)
        {
            try
            {               
                if (boardSize < 1 || boardSize > 10)
                    return null;

                Board board = new Board() { Rows = boardSize, Columns = boardSize };

                //create board
                for (int x = 0; x < board.Rows + 1; x++)
                {
                    for (int y = 0; y < board.Columns + 1; y++)
                    {
                        if (x == 0 && y == 0)
                        {
                            Console.Write("  ");
                            continue;
                        }
                        if (x == 0)
                        {
                            Console.Write(y.ToString() + " ");
                            continue;
                        }

                        if (y == 0)
                            Console.Write(Helper.ConvertIntToAlpha(x - 1) + " ");
                        else
                            Console.Write(" |");
                    }
                    Console.WriteLine("");
                }

                return board;
            }
            catch
            {
                return null;
            }

        }        
    }
}
