﻿using System;
using System.Collections.Generic;
using System.Linq;
using Flare_BattleShip_Interfaces;
using Flare_BattleShip_Models;

namespace Flare_BattleShip_GameLogic
{
    public class BattleShip : IShip
    {
        private ShipSpan _userShip;
        
        public bool AddShip(List<Coordinates> shipCoordinates, int boardSize)
        {
            try
            {
                _userShip = new ShipSpan();
                _userShip.Ship = new List<Ship>();
                _userShip.BoardSize = boardSize;

                if (!ValidateBoardWithShipCoordinates(shipCoordinates))
                {
                    Helper.WriteError("Index out of bounds.");
                    return false;
                }

                shipCoordinates.ForEach(x =>
                {
                    _userShip.Ship.Add(
                        new Ship()
                        {
                            xCoordinate = x.xCoordinate,
                            yCoordinate = x.yCoordinate,
                            IsHit = false
                        });
                });

                return true;
            }
            catch
            {
                return false;
            }
        }

        public Tuple<ShotStatus, bool> Attack(Coordinates shot)
        {
            try
            {
                if (!ValidateBoardWithShipCoordinates(new List<Coordinates> { shot }))
                    return new Tuple<ShotStatus, bool>(ShotStatus.OutOfBounds, false);

                //get the ship if any for the shot coordinates
                var ship = _userShip.Ship.FirstOrDefault(x =>
                                        x.xCoordinate == shot.xCoordinate &&
                                        x.yCoordinate == shot.yCoordinate);
                //if no ship found, it is a miss
                if (ship == null)
                {
                    return new Tuple<ShotStatus, bool>(ShotStatus.Miss, false);
                }
                //ship is found and is already hit
                else if (ship.IsHit)
                {
                    return new Tuple<ShotStatus, bool>(ShotStatus.DuplicateHit, _userShip.IsShipDestroyed);
                }
                //ship is found and is hit
                else
                {
                    ship.IsHit = true;
                    return new Tuple<ShotStatus, bool>(ShotStatus.Hit, _userShip.IsShipDestroyed);
                }
            }
            catch
            {
                return new Tuple<ShotStatus, bool>(ShotStatus.Unknown, _userShip.IsShipDestroyed);
            }
        }

        private bool ValidateBoardWithShipCoordinates(List<Coordinates> coordinates)
        {
            int boardSize = _userShip.BoardSize;

            if (boardSize <= 0 || !coordinates.Any())
                return false;

            return !coordinates.Any(x => x.xCoordinate > boardSize || x.yCoordinate > boardSize);
        }
    }
}
