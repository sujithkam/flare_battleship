﻿
namespace Flare_BattleShip_Models
{
    public class Board
    {
        public int Rows { get; set; }

        public int Columns { get; set; }
    }
}
