﻿
namespace Flare_BattleShip_Models
{
    public class Coordinates
    {
        public int xCoordinate { get; set; }

        public int yCoordinate { get; set; }
    }
}
