﻿using System.Collections.Generic;
using System.Linq;

namespace Flare_BattleShip_Models
{
    public enum ShotStatus
    {
        Hit,
        Miss,
        Unknown,
        OutOfBounds,
        DuplicateHit
    }

    public class ShipSpan
    {
        public string Name { get; set; }

        public int BoardSize { get; set; }

        public List<Ship> Ship { get; set; }

        //if all the coordinate of the ship have been hit
        //then it is assumed that the ship is destroyed 
        public bool IsShipDestroyed => this.Ship.All(x => x.IsHit);
    }
}
