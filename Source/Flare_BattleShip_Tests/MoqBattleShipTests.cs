﻿using System.Collections.Generic;
using Flare_BattleShip_GameLogic;
using Flare_BattleShip_Models;
using Moq;
using Xunit;


namespace Flare_BattleShip_Tests
{
    public class MoqBattleShipTests
    {
        [Fact]
        public void ValidShipAdd()
        {
            //arrange
            var moqBoard = new Mock<BattleShip>();
            var boardSize = 10;
            var shipCoordinates = new List<Coordinates>() { new Coordinates() { xCoordinate = 1, yCoordinate = 1 } };

            //act
            var ship = moqBoard.Object.AddShip(shipCoordinates, boardSize);

            //assert
            Assert.True(ship);
        }

        [Fact]
        public void InValidShipAdd()
        {
            //arrange
            var moqBoard = new Mock<BattleShip>();
            var boardSize = 10;
            var shipCoordinates = new List<Coordinates>() { new Coordinates() { xCoordinate = 11, yCoordinate = 11 } };

            //act
            var ship = moqBoard.Object.AddShip(shipCoordinates, boardSize);

            //assert
            Assert.False(ship);
        }

        [Fact]
        public void ValidShipAttack()
        {
            //arrange
            var moqBoard = new Mock<BattleShip>();
            var boardSize = 10;
            var shipCoordinates = new List<Coordinates>() { new Coordinates() { xCoordinate = 1, yCoordinate = 1 } };
            var shipCreated = moqBoard.Object.AddShip(shipCoordinates, boardSize);

            //act
            var shipAttack = moqBoard.Object.Attack(new Coordinates() { xCoordinate = 1, yCoordinate = 1 });

            //assert
            Assert.Equal(ShotStatus.Hit, shipAttack.Item1);
            Assert.True(shipAttack.Item2);
        }

        [Fact]
        public void InValidShipAttack()
        {
            //arrange
            var moqBoard = new Mock<BattleShip>();
            var boardSize = 10;
            var shipCoordinates = new List<Coordinates>() { new Coordinates() { xCoordinate = 1, yCoordinate = 1 } };
            var shipCreated = moqBoard.Object.AddShip(shipCoordinates, boardSize);

            //act
            var shipAttack = moqBoard.Object.Attack(new Coordinates() { xCoordinate = 2, yCoordinate = 2 });

            //assert
            Assert.Equal(ShotStatus.Miss, shipAttack.Item1);
        }
    }
}
