﻿using Flare_BattleShip_GameLogic;
using Moq;
using Xunit;

namespace Flare_BattleShip_Tests
{   
    
    public class MoqBattleBoardTests
    {
        [Fact]
        public void ValidBoardSize()
        {
            //arrange
            var moqBoard = new Mock<BattleBoard>();
            var boardSize = 10;

            //act
            var board = moqBoard.Object.CreateBoard(boardSize);

            //assert
            Assert.Equal(boardSize, board.Rows);
        }

        [Fact]
        public void InValidBoardSize()
        {
            //arrange
            var moqBoard = new Mock<BattleBoard>();
            var boardSize = 11;

            //act
            var board = moqBoard.Object.CreateBoard(boardSize);

            //assert
            Assert.Null(board);
        }
    }
}
