﻿using Flare_BattleShip_GameLogic;
using Flare_BattleShip_Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Flare_BattleShip
{
    class Program
    {
        static void Main(string[] args)
        {
            //add the interface implementation mappings
            var serviceProvider = new ServiceCollection()
           .AddSingleton<IGame, Game>()
           .AddSingleton<IBattleBoard, BattleBoard>()
           .AddSingleton<IShip, BattleShip>()
           .BuildServiceProvider();

            //get the game service
            var game = serviceProvider.GetService<IGame>();
            game.Start();
        }
    }
}
