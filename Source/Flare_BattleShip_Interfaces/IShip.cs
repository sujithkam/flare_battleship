﻿using Flare_BattleShip_Models;
using System;
using System.Collections.Generic;

namespace Flare_BattleShip_Interfaces
{
    public interface IShip
    {
        bool AddShip(List<Coordinates> shipCoordinates, int boardSize);

        Tuple<ShotStatus, bool> Attack(Coordinates shotCoordinates);
    }
}
