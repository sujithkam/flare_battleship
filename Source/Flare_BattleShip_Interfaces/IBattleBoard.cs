﻿using Flare_BattleShip_Models;

namespace Flare_BattleShip_Interfaces
{
    public interface IBattleBoard
    {        
        Board CreateBoard(int boardSize);
    }
}
