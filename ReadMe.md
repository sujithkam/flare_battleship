The Flare_BattleShip project is a console application that has been created using .NET Core (2.1).

The player can select the board size between 1 and 10.

Once the board is created the player needs to place a battle ship within the board boundary, 
occupying one cell or spanning horizontally or vertically within the board boundary.

Then the player needs to take the shot and the program will respond back with hit, miss , out of bounds.

When all the cells occupied by the ship are destroyed, the player wins the game.

Project dependencies : 
Microsoft.Extensions.DependencyInjection
xUnit    
Moq
